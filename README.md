# Relógio de Cabeceira

Este aplicativo foi criado no curso [Desenvolvedor Android Intermediário](
https://www.udemy.com/desenvolvedor-android-intermediario/). 

Este aplicativo mostra um relógio de acordo com o relógio do celular.

É feita utilização de alguns conceitos interessantes, como Thread, orientação de uma Activity e animações.

Fique à vontade para baixar o código, rodar o aplicativo e se tiver alguma dúvida, basta entrar em contato conosco através do site [DevMasterTeam](http://www.devmasterteam.com/#contact) e mesmo que não esteja no curso, teremos prazer em atendê-lo!

[![](https://github.com/DevMasterTeam/RelogioDeCabeceira/blob/master/presentation/Image1.png)](https://github.com/DevMasterTeam/RelogioDeCabeceira/blob/master/presentation/Image1.png)

[![](https://github.com/DevMasterTeam/RelogioDeCabeceira/blob/master/presentation/Image2.png)](https://github.com/DevMasterTeam/RelogioDeCabeceira/blob/master/presentation/Image2.png)